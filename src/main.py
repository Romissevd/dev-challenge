import os
import re

from motor.motor_asyncio import AsyncIOMotorClient
from fastapi import FastAPI, HTTPException
from dotenv import load_dotenv
from starlette.requests import Request
from starlette.responses import JSONResponse

from constant import ERROR_TEXT
from src.schemas import Value
from src.utils import calculate_the_result

load_dotenv()
DATABASE_NAME = os.environ.get('DATABASE_NAME', 'challenge')
MONGODB_HOST = os.environ.get('MONGODB_HOST', 'localhost')
MONGODB_PORT = os.environ.get('MONGODB_PORT', '27017')
MONGODB_URL = f'mongodb://{MONGODB_HOST}:{MONGODB_PORT}/{DATABASE_NAME.lstrip("/")}'
client = AsyncIOMotorClient(MONGODB_URL)

app = FastAPI()
app.state.mongo_client = client['challenge']


@app.get('/{collection}/{var}')
async def get_record(request: Request, collection: str, var: str) -> dict:
    collection = collection.lower()
    var_number = re.findall(r'(?<=\D)\d+|\d+', var)
    if len(var_number) != 1:
        raise HTTPException(status_code=404)
    mongo_client: AsyncIOMotorClient = request.app.state.mongo_client
    mongo_collection = mongo_client[collection]
    cursor = await mongo_collection.find_one({'var': f'var{var_number[0]}'})
    if not cursor:
        raise HTTPException(status_code=404)
    value = cursor.get('value')
    result = await calculate_the_result(value, mongo_collection, var)
    return {
        'value': value,
        'result': result,
    }


@app.post('/{collection}/{var}', status_code=201)
async def create_record(request: Request, collection: str, var: str, value: Value) -> dict:
    collection = collection.lower()
    var_number = re.findall(r'(?<=\D)\d+|\d+', var)
    if len(var_number) != 1:
        return JSONResponse(content={'detail': 'URL error'}, status_code=422)
    mongo_client: AsyncIOMotorClient = request.app.state.mongo_client
    mongo_collection = mongo_client[collection]
    await mongo_collection.find_one_and_update(
        {'var': f'var{var_number[0]}'},
        {'$set': {'value': str(value.value)}},
        upsert=True,
    )
    result = await calculate_the_result(str(value.value), mongo_collection, var)
    response = {'value': str(value.value), 'result': result}
    if ERROR_TEXT in result:
        await mongo_collection.delete_one({'var': f'var{var_number[0]}'})
        return JSONResponse(content=response, status_code=422)
    return response


@app.get('/{collection}')
async def get_collection(request: Request, collection: str) -> dict:
    collection = collection.lower()
    mongo_client: AsyncIOMotorClient = request.app.state.mongo_client
    mongo_collection = mongo_client[collection]
    count_documents = await mongo_collection.count_documents({})
    if not count_documents:
        raise HTTPException(status_code=404)
    result = {}
    async for cursor in mongo_collection.find({}):
        value = cursor.get('value')
        var = cursor.get('var')
        result[var] = {
            'value': value,
            'result': await calculate_the_result(value, mongo_collection, var)
        }
    return result
