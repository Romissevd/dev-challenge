import re

from simpleeval import simple_eval, NameNotDefined
from motor.motor_asyncio import AsyncIOMotorCollection

from constant import ERROR_TEXT


def _replace(match):
    key = match.group(1)
    return f'{{{key}}}'


async def calculate_the_result(value: str, mongo_collection: AsyncIOMotorCollection, request_var: str) -> str:
    if request_var in value:
        return ERROR_TEXT
    if value.startswith('='):
        values = {}
        for var in set(re.findall(r'(var\d+)', value)):
            cursor = await mongo_collection.find_one({'var': var})
            if not cursor:
                return ERROR_TEXT
            cursor_value = str(cursor.get('value'))
            if cursor_value.startswith('='):
                cursor_value = await calculate_the_result(cursor_value, mongo_collection, var)
            values[var] = cursor_value
        result = re.sub(r'(var\d+)', _replace, value)
        result = result.replace('=', '').format(**values)
        if ERROR_TEXT in result:
            return ERROR_TEXT
        try:
            result = str(simple_eval(result))
        except (ZeroDivisionError, NameNotDefined):
            return ERROR_TEXT
    else:
        try:
            float(value)
            result = value
        except ValueError:
            result = ERROR_TEXT
    return result
