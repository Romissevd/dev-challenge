from typing import Union

from pydantic import BaseModel


class Value(BaseModel):
    value: Union[str, int, float]
