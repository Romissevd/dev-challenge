import os

import pytest
from httpx import AsyncClient

from src.main import app
from motor.motor_asyncio import AsyncIOMotorClient


TEST_DB = 'testdb'
NAME_TEST_COLLECTION = 'test'
MONGODB_HOST = os.environ.get('MONGODB_HOST', 'localhost')
MONGODB_PORT = os.environ.get('MONGODB_PORT', '27017')


@pytest.fixture
async def client():
    async with AsyncClient(app=app, base_url='http://testserver') as c:
        yield c


@pytest.fixture
async def db():
    client = AsyncIOMotorClient(f'mongodb://{MONGODB_HOST}:{MONGODB_PORT}/{TEST_DB}')
    app.state.mongo_client = client[TEST_DB]
    yield client[TEST_DB]
    await client[TEST_DB].drop_collection(NAME_TEST_COLLECTION)
