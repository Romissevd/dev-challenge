import pytest

from constant import ERROR_TEXT
from conftest import NAME_TEST_COLLECTION


@pytest.mark.asyncio
async def test_get_not_exists_sheet(client, db):

    response = await client.get(f'/{NAME_TEST_COLLECTION}')

    assert response.status_code == 404


@pytest.mark.asyncio
async def test_get_exists_sheet(client, db):
    data = {'var': 'var1', 'value': '1'}
    await db[NAME_TEST_COLLECTION].insert_one(data)

    response = await client.get(f'/{NAME_TEST_COLLECTION}')

    assert response.status_code == 200
    assert response.json() == {'var1': {'value': '1', 'result': '1'}}


@pytest.mark.asyncio
async def test_get_exists_data(client, db):
    data = {'var': 'var1', 'value': '1'}
    await db[NAME_TEST_COLLECTION].insert_one(data)

    response = await client.get(f'/{NAME_TEST_COLLECTION}/var1')
    assert response.status_code == 200
    assert response.json() == {'value': '1', 'result': '1'}


@pytest.mark.asyncio
async def test_get_not_exists_data(client, db):

    response = await client.get(f'/{NAME_TEST_COLLECTION}/var1')
    assert response.status_code == 404


@pytest.mark.asyncio
async def test_successful_data_creation(client, db):

    response = await client.post(f'/{NAME_TEST_COLLECTION}/var1', json={'value': '1'})
    assert response.status_code == 201
    assert response.json() == {'value': '1', 'result': '1'}


@pytest.mark.parametrize(
    'value', [
        'var1',
        'var1+var2',
        'some_text',
    ]
)
@pytest.mark.asyncio
async def test_failed_data_creation(client, db, value):

    response = await client.post(f'/{NAME_TEST_COLLECTION}/var1', json={'value': value})
    assert response.status_code == 422
    assert response.json() == {'value': value, 'result': ERROR_TEXT}


@pytest.mark.parametrize(
    'var1,var2,value,result', [
        ('1', 2, '=var1+var2', '3'),
        ('1', 2, '=var1-var2', '-1'),
        ('4.5', 2, '=var1*var2', '9.0'),
        ('6', 2, '=var1/var2', '3.0'),
        ('6', 2, '=var1 * (var1-var2)', '24'),
    ]
)
@pytest.mark.asyncio
async def test_get_math_operation(client, db, var1, var2, value, result):
    await db[NAME_TEST_COLLECTION].insert_one({'var': 'var1', 'value': var1})
    await db[NAME_TEST_COLLECTION].insert_one({'var': 'var2', 'value': var2})
    await db[NAME_TEST_COLLECTION].insert_one({'var': 'var3', 'value': value})

    response = await client.get(f'/{NAME_TEST_COLLECTION}/var3')
    assert response.status_code == 200
    assert response.json() == {'value': value, 'result': result}


@pytest.mark.parametrize(
    'var1,var2,value,result', [
        ('1', 2, '=var1+var2', '3'),
        ('1', 2, '=var1-var2', '-1'),
        ('4.5', 2, '=var1*var2', '9.0'),
        ('6', 2, '=var1/var2', '3.0'),
        ('6', 2, '=var1*(var1-var2)', '24'),
    ]
)
@pytest.mark.asyncio
async def test_create_math_operation(client, db, var1, var2, value, result):
    await db[NAME_TEST_COLLECTION].insert_one({'var': 'var1', 'value': var1})
    await db[NAME_TEST_COLLECTION].insert_one({'var': 'var2', 'value': var2})

    response = await client.post(f'/{NAME_TEST_COLLECTION}/var3', json={'value': value})
    assert response.status_code == 201
    assert response.json() == {'value': value, 'result': result}


@pytest.mark.asyncio
async def test_zero_division(client, db):
    await db[NAME_TEST_COLLECTION].insert_one({'var': 'var1', 'value': 5})
    await db[NAME_TEST_COLLECTION].insert_one({'var': 'var2', 'value': 0})

    response = await client.post(f'/{NAME_TEST_COLLECTION}/var3', json={'value': '=var1/var2'})
    assert response.status_code == 422
    assert response.json() == {'value': '=var1/var2', 'result': ERROR_TEXT}


@pytest.mark.asyncio
async def test_error_get_url(client, db):

    response = await client.get(f'/{NAME_TEST_COLLECTION}/var3var1')
    assert response.status_code == 404


@pytest.mark.asyncio
async def test_error_post_url(client, db):

    response = await client.post(f'/{NAME_TEST_COLLECTION}/var3var1', json={'value': '=var1/var2'})
    assert response.status_code == 422
    assert response.json() == {'detail': 'URL error'}


@pytest.mark.asyncio
async def test_not_found_var(client, db):
    await db[NAME_TEST_COLLECTION].insert_one({'var': 'var1', 'value': 5})

    response = await client.post(f'/{NAME_TEST_COLLECTION}/var3', json={'value': '=var1/var2'})
    assert response.status_code == 422
    assert response.json() == {'value': '=var1/var2', 'result': ERROR_TEXT}


@pytest.mark.asyncio
async def test_attached_var(client, db):
    await db[NAME_TEST_COLLECTION].insert_one({'var': 'var1', 'value': 5})
    await db[NAME_TEST_COLLECTION].insert_one({'var': 'var2', 'value': 3})
    await db[NAME_TEST_COLLECTION].insert_one({'var': 'var3', 'value': '=var1+var2'})
    await db[NAME_TEST_COLLECTION].insert_one({'var': 'var4', 'value': '=var1-var2'})

    response = await client.post(f'/{NAME_TEST_COLLECTION}/var5', json={'value': '=var3+var4'})
    assert response.status_code == 201
    assert response.json() == {'value': '=var3+var4', 'result': '10'}
