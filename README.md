# Dev Challenge

Implementation of spreadsheets using API.

### Language and tools
* Python 3.9
* FastAPI
* MongoDB

Run code

```bash
docker compose up
```

Run tests

```bash
docker compose run --rm web pytest tests/test.py
```

Due to the fact that this is a challenge, file `.env` is added to the archive.

### API
After running the code, you can access the API documentation at http://localhost:8080/docs

### Description
The cell at the input can have any name, but at the output in the list it will have the name `var`.

**Example:**

request:

POST ```http://localhost:8080/sheet/cell1```

GET ```http://localhost:8080/sheet```

response:
```json
{
  "var1": {
    "value": "1",
    "result": "1"
  }
}
```

`cell1 -> var1`

### Ideas for improvement
* Add more security
* Would close the possibility of using different cell_ids
* Will extend the functionality by adding the ability to enter values with `ERROR` or future values into the database
* Add ability to delete cells

